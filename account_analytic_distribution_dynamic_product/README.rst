=====================================
Account Analytic Distribution Dynamic
=====================================

This module is only usable in relation with one of the following modules:

*. account_analytic_distribution_dynamic_product

**Table of contents**

.. contents::
   :local:

Configuration
=============

There is nothing to configure here

Usage
=====

This module is only base module for others and deploys no features to use.

Credits
=======

Authors
~~~~~~~

* Jamotion GmbH

Contributors
------------

* Flectra Community <info@flectra-community.org>