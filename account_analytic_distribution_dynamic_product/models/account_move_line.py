from flectra import api, fields, models


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    def _analytic_line_distributed_prepare(self, rule):

        if rule.type != 'product':
            return super()._analytic_line_distributed_prepare(rule)

        res = self._prepare_analytic_line()
        if not res:
            return False

        res = res[0]
        analytic_accounts = self.product_id.product_tmpl_id._get_product_analytic_accounts()

        amount = (res.get('amount') * rule.percent) / 100.0
        if amount > 0:
            analytic_account = analytic_accounts['income']
        else:
            analytic_account = analytic_accounts['expense']
        if not analytic_account:
            return False

        res['amount'] = amount
        res['account_id'] = analytic_account.id

        return res
