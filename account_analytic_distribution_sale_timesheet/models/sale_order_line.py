import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _prepare_invoice_line(self, qty):
        result = super()._prepare_invoice_line(qty)
        if self.product_id and self.product_id.analytic_distribution_id:
            result['analytic_distribution_id'] = self.product_id.analytic_distribution_id.id
        return result
