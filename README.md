# Flectra Community / account-analytic

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[analytic_product_category](analytic_product_category/) | 1.0.1.0.0| Filter and Group Analytic Entries by Product Category
[analytic_partner](analytic_partner/) | 1.0.1.0.0| Search and group analytic entries by partner
[stock_inventory_analytic](stock_inventory_analytic/) | 1.0.1.0.0|         Stock Inventory Analytic 
[account_analytic_required](account_analytic_required/) | 1.0.1.0.0| Account Analytic Required
[account_analytic_default_account](account_analytic_default_account/) | 1.0.1.0.0| Account Analytic Default Account
[mrp_analytic](mrp_analytic/) | 1.0.1.0.0| Adds the analytic account to the production order
[analytic_base_department](analytic_base_department/) | 1.0.1.0.0| Base Analytic Department Categorization
[analytic_tag_dimension_purchase_warning](analytic_tag_dimension_purchase_warning/) | 1.0.1.0.0| Group Analytic Entries by Dimensions
[account_analytic_sequence](account_analytic_sequence/) | 1.0.1.0.0|         Restore the analytic account sequence
[procurement_mto_analytic](procurement_mto_analytic/) | 1.0.1.0.0| This module sets analytic account in purchase order line from sale order analytic account
[account_move_analytic_recreate](account_move_analytic_recreate/) | 1.0.1.0.0| Recreate analytic lines when modifying account moves.
[account_analytic_parent](account_analytic_parent/) | 1.0.2.0.0|         This module reintroduces the hierarchy to the analytic accounts.
[stock_analytic](stock_analytic/) | 1.0.1.0.0| Adds an analytic account in stock move
[product_analytic](product_analytic/) | 1.0.1.0.0| Add analytic account on products and product categories
[analytic_tag_dimension](analytic_tag_dimension/) | 1.0.1.0.0| Group Analytic Entries by Dimensions
[analytic_tag_dimension_sale_warning](analytic_tag_dimension_sale_warning/) | 1.0.1.0.0| Group Analytic Entries by Dimensions
[account_analytic_distribution](account_analytic_distribution/) | 1.0.1.0.0| Distribute incoming/outcoming account moves to several analytic accounts


