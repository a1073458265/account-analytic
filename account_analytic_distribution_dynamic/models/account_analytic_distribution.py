from flectra import _, api, fields, models


class AccountAnalyticDistributionRule(models.Model):
    _inherit = "account.analytic.distribution.rule"

    type = fields.Selection(
            string="Type",
            selection=[
                ('fixed', 'Fixed'),
                ('existing', 'Existing'),
            ],
            default='fixed',
            required=True,
    )
